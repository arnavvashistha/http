  class Comment {
    rating: number;
    comment: string;
    author: string;
    date: string;
}
const RATINGS = [1,2,3,4,5];
export {Comment,RATINGS};
